#ifndef BUTTON_H
#define BUTTON_H

//=================================  
// Constants

//=================================  
// forward declared dependencies  

//=================================  
// included dependencies
#include <string>
#include "library.h"
#include <SFML/Graphics.hpp>

class Button : protected The_font, protected Sound_lib {
public:
	Button() = delete;
	Button(sf::RenderWindow& window, std::string texts, sf::Vector2i pos, int textsize = 30, std::string clickeffect = "");
	void setText(std::string text, bool reAlign = true);
	void update(std::vector<sf::Event>& events);
	bool isPressed();
	void draw();
private:
	sf::RenderWindow& window;
	int size;
	sf::Vector2i mousepos;
	sf::Text text;
	sf::Vector2f pos;
	std::string clickSound;
	bool playHoverSound;
	bool pressed;
	bool hovered;
};
#endif
