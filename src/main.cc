#include <SFML/Graphics.hpp>
#include <vector>
#include "library.h"
#include "menu.h"
#include <exception>
#include <iostream>

int main() {

	sf::RenderWindow window(sf::VideoMode(1400, 800), "Ray Fighters", sf::Style::Titlebar | sf::Style::Close);
	//window.setFramerateLimit(60); Lol peasants
	sf::FloatRect visibleArea(0.0f, 0.0f, static_cast<float>(window.getSize().x), static_cast<float>(window.getSize().y));
	window.setView(sf::View(visibleArea));

	sf::Clock gameClock{};

	{
		//Game has its own bracket to make sure it deconstruct before static stuff
		Menu game{ window };
		while (window.isOpen()) {
			if (gameClock.getElapsedTime().asMilliseconds() >= 16.66) {
				gameClock.restart();
				std::vector<sf::Event> events;
				sf::Event event;
				while (window.pollEvent(event)) {
					if (event.type == sf::Event::Closed) {
						window.close();
						return 0;
					}
					else if (event.type == sf::Event::Resized) {
						// update the view to the new size of the window
						sf::FloatRect visibleArea(0, 0, static_cast<float>(event.size.width), static_cast<float>(event.size.height));
						window.setView(sf::View(visibleArea));
					}
					events.push_back(event);
				}

				window.clear(sf::Color::White);
				// update game events
				try {
					game.update(events);
					game.draw();
				}
				catch (const std::exception& e) {
					std::cout << e.what() << std::endl;
					return 1;
				}
				// Draw stuff on screen 
				window.display();
			}
		}
	}
	Sound_lib::Empty();
	return 0;
}
