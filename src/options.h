#ifndef OPTIONS_H
#define OPTIONS_H

//=================================  
// Constants

//=================================  
// forward declared dependencies  

//=================================  
// included dependencies
#include <SFML/Window.hpp>
#include <vector>
#include <map>
#include <array>
#include "button.h"
#include "game_state.h"
#include "library.h"
#include "controller.h"

class Options : protected The_font, protected Game_State, protected Sound_lib {
public:
	Options(sf::RenderWindow& _window);
	~Options();
	void update(std::vector<sf::Event>& events);
	void draw();
	Controllers& get_controller();

	//Hidden variables
	Controllers cntr;
private:
	sf::RenderWindow& window;
	sf::RectangleShape Background, Volbar, Volbox;
	Button returnButton, pbutton, mbutton;
	std::array<std::unique_ptr<Button>, Controller::key_enum::last> p1buttons;
	std::array<std::unique_ptr<Button>, Controller::key_enum::last> p2buttons;
	std::vector<sf::Text> texts;
	std::vector<sf::Text> waitForKeyTexts;
	bool waitForKey, player1;
	Controller::key_enum selectedKey;
	std::map<sf::Keyboard::Key, std::string> lookupKey;

	sf::Text createText(std::string, sf::Vector2i);
	bool assignJoystickButtons(int);
	void assignJoystickAxis(int);
};


#endif
