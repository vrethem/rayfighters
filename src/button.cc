#include "button.h"

Button::Button(sf::RenderWindow& _window, std::string texts, sf::Vector2i po, int textsize, std::string clickeffect) :
	window{ _window },
	size{},
	mousepos{},
	text{},
	pos{ po },
	clickSound{},
	playHoverSound{ true },
	pressed{ false },
	hovered{ false } {
	if (clickeffect != "") {
		clickSound = "Sound_effects/" + clickeffect;
	}
	text.setFont(getFont());
	text.setString(texts);
	text.setFillColor(sf::Color::Red);
	text.setCharacterSize(textsize);
	text.setStyle(sf::Text::Bold);
	text.setOutlineThickness(6);
	text.setOutlineColor(sf::Color::Black);
	auto tmp = text.getGlobalBounds();
	text.setPosition(pos.x - tmp.width / 2, pos.y - tmp.height / 2);
	size = textsize;
}

void Button::setText(std::string s, bool reAlign) {
	text.setString(s);
	auto tmp = text.getGlobalBounds();
	if (reAlign) {
		text.setPosition(pos.x - tmp.width / 2, pos.y - tmp.height / 2);
	}
}

void Button::update(std::vector<sf::Event>& events) {
	mousepos = sf::Mouse::getPosition(window);
	sf::Rect<int> rect{ window.mapCoordsToPixel(text.getPosition()).x,window.mapCoordsToPixel(text.getPosition()).y + size / 3,static_cast<int>(text.getGlobalBounds().width),static_cast<int>(text.getGlobalBounds().height) };
	hovered = rect.contains(mousepos);
	pressed = hovered && sf::Mouse::isButtonPressed(sf::Mouse::Left);
	if (pressed && clickSound != "") {
		playSound(clickSound, 30);
	}
}

bool Button::isPressed() {
	return pressed;
}

void Button::draw() {
	if (hovered) {
		if (playHoverSound)
			playSound("Sound_effects/hover.wav", 30);
		text.setOutlineThickness(10.0f);
		playHoverSound = false;
	}
	else {
		playHoverSound = true;
		text.setOutlineThickness(6.0f);
	}

	window.draw(text);
}
